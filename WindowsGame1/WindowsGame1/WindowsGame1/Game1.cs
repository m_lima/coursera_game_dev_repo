using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace WindowsGame1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //teddy bear drawing support
        Texture2D bear0;
        Texture2D bear1;
        Texture2D bear2;

        Rectangle rect0;
        Rectangle rect1;
        Rectangle rect2;

        Random rand;

        int screenWidth = 800;
        int screenHeight = 600;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = screenWidth;
            graphics.PreferredBackBufferHeight = screenHeight;

            rand = new Random();
        }

        private int getRandom(int val) {            
            int ret = rand.Next(0, val);
            Console.WriteLine(ret);
            return ret;
        }

        private void drawBears() {
            rect0 = new Rectangle(getRandom(screenWidth), getRandom(screenHeight), bear0.Width, bear0.Height);
            rect1 = new Rectangle(getRandom(screenWidth), getRandom(screenHeight), bear1.Width, bear1.Height);
            rect2 = new Rectangle(getRandom(screenWidth), getRandom(screenHeight), bear2.Width, bear2.Height);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            bear0 = Content.Load<Texture2D>("teddybear0");
            bear1 = Content.Load<Texture2D>("teddybear1");
            bear2 = Content.Load<Texture2D>("teddybear2");

            SpriteBatch spriteBatch = new SpriteBatch(GraphicsDevice);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
                        
            spriteBatch.Begin();
            drawBears();
            spriteBatch.Draw(bear0, rect0, Color.White);
            spriteBatch.Draw(bear1, rect1, Color.White);
            spriteBatch.Draw(bear2, rect2, Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}

