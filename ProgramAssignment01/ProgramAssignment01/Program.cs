﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProgramAssignment01
{
    class Program
    {
        private const float g = 9.8f;

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome!");
            Console.WriteLine("This application will calculate the maximum height of the shell and the distance it will travel along the ground.");
            Console.WriteLine();

            Console.Write("Please enter the initial angle in degrees:");
            float theta = convertDegreesToRadians(float.Parse(Console.ReadLine()));

            Console.Write("Please enter the initial speed:");
            float speed = float.Parse(Console.ReadLine());

            // Calculating the x component of the velocity at start
            float vox = speed * (float)Math.Cos(theta);

            // Calculating the y component of the velocity at start
            float voy = speed * (float)Math.Sin(theta);

            //Calculating t
            float t = voy / g;

            float h = voy * voy / (2 * g);

            float dx = vox * 2 * t;

            Console.WriteLine("Maximum shell height: " + h);
            Console.WriteLine("Horizontal distance: " + dx);

            Console.WriteLine();
        }

        public static float convertDegreesToRadians(float degree)
        {
            return (float)(2 * Math.PI * degree) / 360;
        }
    }
}

