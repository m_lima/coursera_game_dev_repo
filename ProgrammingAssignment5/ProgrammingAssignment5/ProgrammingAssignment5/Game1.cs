using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using TeddyMineExplosion;

namespace ProgrammingAssignment5
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        const int WINDOW_WIDTH = 800;
        const int WINDOW_HEIGHT = 600;

        int SPAWN_TEDDYBEAR_TIMER = 0;
        int SPAWN_TEDDYBEAR_DELAY = 0;
        Random rand = new Random();

        bool leftClickReleased = true;

        Texture2D mineSprite;

        Texture2D explosionSprite;

        Texture2D teddySprite;

        List<Mine> listMines = new List<Mine>();
        List<TeddyBear> listTeddies = new List<TeddyBear>();
        List<Explosion> listExplosions = new List<Explosion>();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = WINDOW_WIDTH;
            graphics.PreferredBackBufferHeight = WINDOW_HEIGHT;

            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            teddySprite = Content.Load<Texture2D>("teddybear");
            mineSprite = Content.Load<Texture2D>("mine");
            explosionSprite = Content.Load<Texture2D>("explosion");

            SPAWN_TEDDYBEAR_DELAY = getRandomSpawnDelayTeddyBear();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            MouseState mouse = Mouse.GetState();

            if (mouse.LeftButton == ButtonState.Pressed && leftClickReleased)
            {
                leftClickReleased = false;
                listMines.Add(new Mine(mineSprite, mouse.X, mouse.Y));
            }
            else
            {
                leftClickReleased = true;
            }

            SPAWN_TEDDYBEAR_TIMER += gameTime.ElapsedGameTime.Milliseconds;
            if (SPAWN_TEDDYBEAR_TIMER >= SPAWN_TEDDYBEAR_DELAY)
            {
                SPAWN_TEDDYBEAR_TIMER = 0;
                SPAWN_TEDDYBEAR_DELAY = getRandomSpawnDelayTeddyBear();

                Vector2 velocity = getRandomVelocity();

                TeddyBear teddy = new TeddyBear(teddySprite, velocity, WINDOW_WIDTH, WINDOW_HEIGHT);
                listTeddies.Add(teddy);
            }

            for (int i = listTeddies.Count - 1; i >= 0; i--) 
            {
                for (int j = listMines.Count - 1; j >= 0; j--)
                {
                    if (listTeddies[i].CollisionRectangle.Intersects(listMines[j].CollisionRectangle))
                    {
                        listTeddies[i].Active = false;
                        listMines[j].Active = false;
                        Explosion explosion = new Explosion(explosionSprite, listMines[j].CollisionRectangle.Center.X, listMines[j].CollisionRectangle.Center.Y);
                        listExplosions.Add(explosion);
                        listMines.RemoveAt(j);
                    }
                }
                if (listTeddies[i].Active)
                {
                    listTeddies[i].Update(gameTime);
                }
                else
                {
                    listTeddies.RemoveAt(i);
                }
            }


            for (int i = listExplosions.Count - 1; i >= 0; i--)
            {
                if (listExplosions[i].Playing)
                {
                    listExplosions[i].Update(gameTime);
                }
                else
                {
                    listExplosions.RemoveAt(i);
                }
            }

                base.Update(gameTime);
        }

        private Vector2 getRandomVelocity()
        {
            double x = -0.5 + rand.NextDouble() * (0.5 - (-0.5));
            double y = -0.5 + rand.NextDouble() * (0.5 - (-0.5));
            
            //Prevents from static teddy bear velocity
            while (x == 0 && y == 0)
            {
                getRandomVelocity();
            }

            Vector2 velocity = new Vector2((float)x, (float)y);
            return velocity;
        }

        private int getRandomSpawnDelayTeddyBear() 
        {
            return rand.Next(1000, 3001);
        }


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            foreach(Mine m in listMines) 
            {
                m.Draw(spriteBatch);
            }

            foreach (TeddyBear t in listTeddies)
            {
                t.Draw(spriteBatch);
            }

            foreach (Explosion e in listExplosions)
            {
                e.Draw(spriteBatch);
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
